import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerTreeComponent } from './worker-tree.component';

describe('WorkerTreeComponent', () => {
  let component: WorkerTreeComponent;
  let fixture: ComponentFixture<WorkerTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkerTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
