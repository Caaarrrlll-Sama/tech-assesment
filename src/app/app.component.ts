import { Component, OnInit } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Manager, Employee, Trainee } from './modules/emp'
import { DataServiceService } from './services/data-service.service'
import { Observable, combineLatest, BehaviorSubject } from 'rxjs'
import { map, debounceTime } from 'rxjs/operators'

@Component({
  selector: `app-root`,
  templateUrl: `./app.component.html`,
  styleUrls: [`./app.component.scss`]
})
export class AppComponent implements OnInit {
  // forms for the display page
  formAdd: FormGroup
  formEdit: FormGroup
  formSearch: FormGroup
  width: number
  // workers is a sorted structured list that contains the workers
  workers: any[] = []
  // allWorkers is a list that contains all the employees in a sequntial list
  allWorkers: any[] = []

  topMan: Manager
  topEmp: Employee
  topTrain: Trainee

  // create a PubSubQueue for worker objects
  workerSubject: BehaviorSubject<any> = new BehaviorSubject([])
  searchSubject: BehaviorSubject<string> = new BehaviorSubject('')
  searchDateSubject: BehaviorSubject<Date> = new BehaviorSubject(new Date('1900-01-01'))
  workerObservable: Observable<any[]>

  constructor(private data: DataServiceService) {
    this.buildForms()
  }

  async ngOnInit() {
    this.width = window.innerWidth
    await this.loadAllForSearch()

    // observe the latest changes from searchfield and workersubject
    this.workerObservable = combineLatest(this.searchSubject, this.workerSubject)
      .pipe(
        // map searchfield and worker values to filtered workers
        map(([searchVal, allWorkers]) => {
          searchVal = searchVal
          return allWorkers.filter(worker => {
            return worker.name.toLowerCase().includes(searchVal.toLowerCase())
            || worker.surname.toLowerCase().includes(searchVal.toLowerCase())
          })
        })
      )
    // Forward search field values to search subject
    this.formSearch.controls.searchField.valueChanges.pipe(debounceTime(100))
      .subscribe(
        value => this.searchSubject.next(value)
      )

    this.formSearch.controls.searchBirthDate.valueChanges.pipe(debounceTime(100))
      .subscribe(
        value => this.searchDateSubject.next(value)
      )
  }

  // build the active controls in the forms
  buildForms() {
    this.formAdd = new FormGroup({
      fname: new FormControl(null, Validators.required),
      sname: new FormControl(null, Validators.required),
      birthDate: new FormControl(null, Validators.required),
      empNo: new FormControl(null, Validators.required),
      salary: new FormControl(null, Validators.required),
      role: new FormControl(null, Validators.required),
      reportsTo: new FormControl(null, Validators.required)
    })

    this.formEdit = new FormGroup({
      fname: new FormControl(null, Validators.required),
      sname: new FormControl(null, Validators.required),
      birthDate: new FormControl(null, Validators.required),
      empNo: new FormControl(null, Validators.required),
      salary: new FormControl(null, Validators.required),
      role: new FormControl(null, Validators.required),
      reportsTo: new FormControl(null, Validators.required)
    })

    this.formSearch = new FormGroup({
      searchField: new FormControl(' ', Validators.required),
      searchBirthDate: new FormControl(null, Validators.required)
    })
  }

  // requests all the data from the database and parses it to objects
  async loadAllForSearch() {
    this.workers = []
    const allUsers = await this.data.readMultiple()
    allUsers.forEach(element => {
      this.createObject(element)
    })

    this.allWorkers = this.workers
    // publish latest worker values into worker subject
    this.workerSubject.next(this.allWorkers)

    // build the workers tree structure in the workers array
    this.workers.forEach(element => {
      element.underlingsIDs.forEach(elem => {
        const found = this.workers.filter(inner => inner.empNo === elem)[0]
        if (found !== undefined) {
          element.underlings.push(found)
        }
        this.workers = this.workers.filter(inner => inner.empNo !== elem)
      })
    })

    this.formSearch.patchValue({
      searchField: ''
    })

    await this.loadTopEarners()
  }

  // collect and store the highest earning members of each set
  async loadTopEarners() {
    const mans = this.allWorkers.filter(element => element.role === `Manager`)
    const emps = this.allWorkers.filter(element => element.role === 'Employee')
    const trains = this.allWorkers.filter(element => element.role === 'Trainee')

    this.topMan = mans[0]
    this.topEmp = emps[0]
    this.topTrain = trains[0]

    mans.forEach(element => {
      if (element.salary > this.topMan.salary) {
        this.topMan = element
      }
    })

    emps.forEach(element => {
      if (element.salary > this.topEmp.salary) {
        this.topEmp = element
      }
    })

    trains.forEach(element => {
      if (element.salary > this.topTrain.salary) {
        this.topTrain = element
      }
    })
  }

  // parses the data from the database into the appropriot object
  createObject(data: any) {
    switch (data.role) {
      case `Manager`: {
        const newUser = new Manager()
        newUser.name = data.name
        newUser.surname = data.surname
        newUser.birthDate = data.birthDate.toDate()
        newUser.empNo = data.empNo
        newUser.salary = data.salary
        newUser.role = `Manager`
        newUser.reportsTo = 0
        newUser.underlingsIDs = data.underlings
        this.workers.push(newUser)
        break
      }
      case `Employee`: {
        const newUser = new Employee()
        newUser.name = data.name
        newUser.surname = data.surname
        newUser.birthDate = data.birthDate.toDate()
        newUser.empNo = data.empNo
        newUser.salary = data.salary
        newUser.role = `Employee`
        newUser.reportsTo = data.reportsTo
        newUser.underlingsIDs = data.underlings
        this.workers.push(newUser)
        break
      }
      case `Trainee`: {
        const newUser = new Trainee()
        newUser.name = data.name
        newUser.surname = data.surname
        newUser.birthDate = data.birthDate.toDate()
        newUser.empNo = data.empNo
        newUser.salary = data.salary
        newUser.role = `Trainee`
        newUser.reportsTo = data.reportsTo
        newUser.underlingsIDs = data.underlings
        this.workers.push(newUser)
        break
      }
    }
  }

  // creates a user of the correct type and passes it to the data service
  async createUser() {
    const c = this.formAdd.controls
    switch (c.role.value) {
      case `Manager`: {
        const newUser = new Manager()
        newUser.name = c.fname.value
        newUser.surname = c.sname.value
        newUser.birthDate = c.birthDate.value.toDate()
        newUser.empNo = parseInt(c.empNo.value, 10)
        newUser.salary = c.salary.value
        newUser.role = `Manager`
        newUser.reportsTo = 0
        const res = await this.data.addUser(newUser)
        if (res !== true) {
          console.log(`User already Exists`)
        }
        break
      }
      case `Employee`: {
        const newUser = new Employee()
        newUser.name = c.fname.value
        newUser.surname = c.sname.value
        newUser.birthDate = c.birthDate.value.toDate()
        newUser.empNo = parseInt(c.empNo.value, 10)
        newUser.salary = c.salary.value
        newUser.role = `Employee`
        newUser.reportsTo = parseInt(c.reportsTo.value, 10)
        const res = await this.data.addUser(newUser)
        if (res !== true) {
          console.log(`User already Exists`)
        }
        break
      }
      case `Trainee`: {
        const newUser = new Trainee()
        newUser.name = c.fname.value
        newUser.surname = c.sname.value
        newUser.birthDate = c.birthDate.value.toDate()
        newUser.empNo = parseInt(c.empNo.value, 10)
        newUser.salary = c.salary.value
        newUser.role = `Trainee`
        newUser.reportsTo = parseInt(c.reportsTo.value, 10)
        const res = await this.data.addUser(newUser)
        if (res !== true) {
          console.log(`User already Exists`)
        }
        break
      }
    }

    this.workers = []
    this.loadAllForSearch()
    this.buildForms()
  }

  // take the user date and populate the update fields
  editUser(empNo: number) {
    const edit = this.allWorkers.find(e => e.empNo === empNo)
    this.formEdit.patchValue({
      fname: edit.name,
      sname: edit.surname,
      birthDate: edit.birthDate,
      empNo: edit.empNo,
      salary: edit.salary,
      role: edit.role,
      reportsTo: edit.reportsTo,
    })
    this.formEdit.controls.empNo.disable()
    this.formEdit.controls.role.disable()
    this.formEdit.controls.reportsTo.disable()
  }

  // save the updated user into the database and reload the information
  async saveUser()  {
    const c = this.formEdit.controls
    switch (c.role.value) {
      case `Manager`: {
        const newUser = new Manager()
        newUser.name = c.fname.value
        newUser.surname = c.sname.value
        newUser.birthDate = c.birthDate.value instanceof Date ? c.birthDate.value : c.birthDate.value.toDate()
        newUser.empNo = parseInt(c.empNo.value, 10)
        newUser.salary = c.salary.value
        newUser.role = `Manager`
        newUser.reportsTo = 0
        newUser.underlingsIDs = this.allWorkers.find(w => w.empNo === newUser.empNo).underlingsIDs
        const res = await this.data.editUser(newUser)
        break
      }
      case `Employee`: {
        const newUser = new Employee()
        newUser.name = c.fname.value
        newUser.surname = c.sname.value
        newUser.birthDate = c.birthDate.value instanceof Date ? c.birthDate.value : c.birthDate.value.toDate()
        newUser.empNo = parseInt(c.empNo.value, 10)
        newUser.salary = c.salary.value
        newUser.role = `Employee`
        newUser.reportsTo = parseInt(c.reportsTo.value, 10)
        newUser.underlingsIDs = this.allWorkers.find(w => w.empNo === newUser.empNo).underlingsIDs
        const res = await this.data.editUser(newUser)
        break
      }
      case `Trainee`: {
        const newUser = new Trainee()
        newUser.name = c.fname.value
        newUser.surname = c.sname.value
        newUser.birthDate = c.birthDate.value instanceof Date ? c.birthDate.value : c.birthDate.value.toDate()
        newUser.empNo = parseInt(c.empNo.value, 10)
        newUser.salary = c.salary.value
        newUser.role = `Trainee`
        newUser.reportsTo = parseInt(c.reportsTo.value, 10)
        const res = await this.data.editUser(newUser)
        break
      }
    }

    this.workers = []
    this.loadAllForSearch()
    this.buildForms()
  }
}
