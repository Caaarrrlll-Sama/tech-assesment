// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAgzYzbMj-r8cTTt4dxhSFxO4S-KgBa9Jk",
    authDomain: "tech-assesment.firebaseapp.com",
    databaseURL: "https://tech-assesment.firebaseio.com",
    projectId: "tech-assesment",
    storageBucket: "",
    messagingSenderId: "273005550477",
    appId: "1:273005550477:web:6cfe3a82a7608b510d3bc4"
  },
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
