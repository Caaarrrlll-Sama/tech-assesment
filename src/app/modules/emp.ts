export abstract class Emp {
  name: string
  surname: string
  birthDate: Date
  empNo: number
  salary: number
  role: `Trainee` | `Employee` | `Manager`
  reportsTo: number
  underlingsIDs: number[] = []
  underlings: any[] = []

  abstract addUnderling(underling: number): void
  abstract removeUnderling(underling: number): void
  abstract promote(): Emp
  abstract serialize(): any
  // think of more!!!
}

export class Manager extends Emp {
  addUnderling(underling: number): void {
    // underling.reportsTo = this.empNo
    this.underlingsIDs.push(underling)
  }
  removeUnderling(underling: number): void {
    this.underlings.splice(this.underlings.findIndex(u => u === underling), 1)
  }
  promote(): Manager {
    console.log(`The manager is the highest role`)
    return this
  }

  serialize(): any {
    return {
      name: this.name,
      surname: this.surname,
      birthDate: this.birthDate,
      empNo: this.empNo,
      salary: this.salary,
      role: this.role,
      reportsTo: this.reportsTo,
      underlings: this.underlingsIDs
    }
  }
}

export class Employee extends Emp {
  addUnderling(underling: number): void {
    // save the ID of the appropriot underling
    this.underlingsIDs.push(underling)
  }
  removeUnderling(underling: number): void {
    this.underlings.splice(this.underlings.findIndex(u => u === underling), 1)
  }
  promote(): Manager {
    const res = new Manager()
    res.name = this.name
    res.surname = this.surname
    res.birthDate = this.birthDate
    res.empNo = this.empNo
    res.salary = this.salary
    res.role = `Manager`
    res.reportsTo = 0
    res.underlings = this.underlingsIDs

    return res
  }

  serialize(): any {
    return {
      name: this.name,
      surname: this.surname,
      birthDate: this.birthDate,
      empNo: this.empNo,
      salary: this.salary,
      role: this.role,
      reportsTo: this.reportsTo,
      underlings: this.underlingsIDs
    }
  }
}

export class Trainee extends Emp {
  addUnderling(): void {
    console.log(`Trainees cannot have underlings`)
  }
  removeUnderling(): void {
    console.log(`Trainees don't have underlings`)
  }
  promote(): Employee {
    const res = new Employee()
    res.name = this.name
    res.surname = this.surname
    res.birthDate = this.birthDate
    res.empNo = this.empNo
    res.salary = this.salary
    res.role = `Employee`
    res.reportsTo = this.reportsTo

    return res
  }

  serialize(): any {
    return {
      name: this.name,
      surname: this.surname,
      birthDate: this.birthDate,
      empNo: this.empNo,
      salary: this.salary,
      role: this.role,
      reportsTo: this.reportsTo,
      underlings: this.underlingsIDs
    }
  }
}
