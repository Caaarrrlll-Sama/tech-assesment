import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { MatToolbarModule, MatIconModule, MatListModule, MatButtonModule, MatTabsModule, MatTreeModule, MatChipsModule, MatGridListModule } from '@angular/material'
import { MatSidenavModule, MatExpansionModule, MatFormFieldModule } from '@angular/material'
import { MatOptionModule, MatSelectModule, MatInputModule } from '@angular/material'
import { MatDatepickerModule } from '@angular/material'

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatExpansionModule,
    MatInputModule,
    MatDatepickerModule,
    MatButtonModule,
    MatTabsModule,
    MatTreeModule,
    MatChipsModule,
    MatGridListModule,

  ],
  exports: [
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatExpansionModule,
    MatInputModule,
    MatDatepickerModule,
    MatButtonModule,
    MatTabsModule,
    MatTreeModule,
    MatChipsModule,
    MatGridListModule,

  ]
})
export class MaterialModule { }
