import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { FlatTreeControl } from '@angular/cdk/tree'
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material'

interface Worker {
  name: string
  surname: string
  birthDate: Date
  empNo: number
  salary: number
  role: `Trainee` | `Employee` | `Manager`
  reportsTo: number
  underlingsIDs: number[]
  underlings: Worker[]
}

interface ExampleFlatNode {
  expandable: boolean
  name: string
  level: number
  empNo: number
  surname: string
  birthDate: Date
  salary: number
  role: `Trainee` | `Employee` | `Manager`
}

@Component({
  selector: 'app-worker-tree',
  templateUrl: './worker-tree.component.html',
  styleUrls: ['./worker-tree.component.scss']
})
export class WorkerTreeComponent implements OnInit {

  workerInit: any = []
  expanded: any[] = []

  @Output() editWorker = new EventEmitter<number>()

  @Input()
  set workers(value: any[]) {

    console.log('setting workers', value)

    if (this.dataSource !== null && this.dataSource !== undefined) {
      this.dataSource.data = value
    } else {
      this.workerInit = value
    }
  }

  treeControl: FlatTreeControl<ExampleFlatNode>
  treeFlattener: MatTreeFlattener<Worker, any>
  dataSource: MatTreeFlatDataSource<Worker, any>

  constructor() { }

  ngOnInit() {
    this.treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable)

    this.treeFlattener = new MatTreeFlattener(
      this.transformer, node => node.level, node => node.expandable, node => node.underlings)

    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener)

    this.dataSource.data = this.workerInit
  }

  private transformer = (node: Worker, level: number) => {
    return {
      expandable: !!node.underlings && node.underlings.length > 0,
      name: node.name,
      level,
      empNo: node.empNo,
      role: node.role,
      surname: node.surname,
      birthDate: node.birthDate,
      salary: node.salary,
    }
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable
}
