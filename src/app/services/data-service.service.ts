import { Injectable } from '@angular/core'
import { Emp, Manager, Employee, Trainee } from '../modules/emp'
import { AngularFirestore } from '@angular/fire/firestore'

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  // shorten the collection call
  fb = this.firestore.collection('workers')
  workers = []

  async serialize(addWorker: Emp) {
    // store the new user in the database
    await this.fb.doc(`${addWorker.empNo}`).set(addWorker.serialize(), { merge: true })
  }

  async readSingle(workerID: number) {
    // read the user with a specific ID from the database
    const res = await this.fb.doc(`${workerID.toString()}`).get().toPromise()
    return res.data()
  }

  async readMultiple() {
    // read all users from the database
    const res = await this.firestore.collection<any>('workers').get().toPromise()
    const mapped = res.docs.map(doc => doc.data())
    // console.log(mapped)
    return mapped
  }

  constructor(private firestore: AngularFirestore) { }

  async editUser(editEmp: Emp) {
    this.serialize(editEmp)
  }

  async addUser(addEmp: Emp) {
    // check if userID already exists meaning the user is already in the database
    const checkUser = await this.readSingle(addEmp.empNo)

    if (checkUser === undefined) {
      // user doesnt exist
      if (addEmp.role === `Manager`) {
        // if new user is manager, he/she will have no underlings nor someone to report to
        this.serialize(addEmp)
        return true
      } else {
        // find superior and create object
        // add the new user to the superiors list
        const superior = await this.readSingle(addEmp.reportsTo)
        let update

        switch (superior.role) {
          case `Manager`: {
            update = new Manager()
            update.name = superior.name
            update.surname = superior.surname
            update.birthDate = superior.birthDate.toDate()
            update.empNo = superior.empNo
            update.salary = superior.salary
            update.role = `Manager`
            update.reportsTo = 0
            update.underlingIDs = superior.underlings
            update.addUnderling(addEmp.empNo)
            break
          }
          case `Employee`: {
            update = new Employee()
            update.name = superior.name
            update.surname = superior.surname
            update.birthDate = superior.birthDate.toDate()
            update.empNo = superior.empNo
            update.salary = superior.salary
            update.role = `Employee`
            update.reportsTo = superior.reportsTo
            update.underlingsIDs = superior.underlings
            update.addUnderling(addEmp.empNo)
            break
          }
        }

        // add new employee and updated supervisor
        this.serialize(addEmp)
        this.serialize(update)

        return true
      }
    } else {
      // user already exists
      // reset page and tell the person the user already exists
      return false
    }
  }
}
